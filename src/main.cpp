#include <iostream>
#include <unistd.h>
////////////Functions and enum///////////////////////
enum Eoperator{plus, minus, over, times, wrong, wrongNumber};
void setOperator(std::string input, Eoperator *word);
void setNumber(std::string, float *Number);
/////////////////////////////////////////////////////

int main()
{
    Eoperator operation; //Enum to set the operator
    float numbers[2]{0,0}; //Numbers to do the operation with
    std::string input;   //User input
    bool bRetry;
    do
    {
    #ifdef WINDOWS
        std::system("cls");
    #else
        std::system ("clear");
    #endif
        bRetry = false;
        std::cout << "write a number, a operator and a number" << std::endl;

        //if statements to set up the calculations. 
        //Doing it this way makes it possible to write everything in one sentence
        // or line by line. 
        for (int i = 0; i<3; i++)
        {
            std::cin >> input;
            if(i==0)
                setNumber(input, &numbers[i]);
            else if(i==1)
                setOperator(input, &operation);
            else if(i==2)
                setNumber(input, &numbers[i-1]);
        }
        //Detecting error in case of wrong number
        if(numbers[0] == 404 || numbers[1] == 404)
            operation = Eoperator::wrongNumber;
        //Prints a calculation depending on the operator set by the user.
        switch(operation)
        {
            case plus:
               std::cout << numbers[0] + numbers[1] << std::endl;
               break;
           case minus:
                std::cout << numbers[0] - numbers[1] << std::endl;
                break;
            case over:
                std::cout << numbers[0] / numbers[1] << std::endl;
                break;
            case times:
                std::cout << numbers[0] * numbers[1] << std::endl;
                break;
            case wrong: 
            case wrongNumber:
                try
                {
                    if(operation == Eoperator::wrong)
                        throw Eoperator::wrong; //if operator was wrong
                    if(operation == Eoperator::wrongNumber)
                        throw 404; //if number was wrong
                }
                catch(Eoperator wrong)
                {
                    std::cout << "You typed the operator wrong. Error: " << wrong << std::endl;
                    bRetry = true;
                }
                catch(const int problemNumber)
                {
                    std::cout << "A number was written wrong. Number input: " << numbers[0] << " and " << numbers[1] << std::endl;
                    bRetry = true;
                }
                sleep(2);
                std::cout << "Restarting..." << std::endl;
                sleep(5);
                break;
    }
    }while(bRetry);
}
void setOperator(std::string input, Eoperator *operation)
{
    //Setting the operator for the calculation depending on input
    if(input == "plus" || input == "+")
    {
        *operation = Eoperator::plus;
    }
    else if(input == "minus" || input == "-")
    {
        *operation = Eoperator::minus;
    }
    else if(input == "over" || input == "/")
    {
        *operation = Eoperator::over;
    }
    else if(input == "times" || input == "*")
    {
        *operation = Eoperator::times;
    }
    else
    {
        *operation = Eoperator::wrong;
    }
}
void setNumber(std::string input, float *number)
{
    //setting up the numbers depending on input
    if(input == "one" || input == "1")
    {
        *number = 1;
    }
    else if(input == "two" || input == "2")
    {
        *number = 2;
    }
    else if(input == "three" || input == "3")
    {
        *number = 3;
    }
    else if(input == "four" || input == "4")
    {
        *number = 4;
    }
    else if(input == "five" || input == "5")
    {
        *number = 5;
    }
    else if(input == "six" || input == "6")
    {
        *number = 6;
    }
    else if(input == "seven" || input == "7")
    {
        *number = 7;
    }
    else if(input == "eight" || input == "8")
    {
        *number = 8;
    }
    else if(input == "nine" || input == "9")
    {
        *number = 9;
    }
    else if(input == "ten" || input == "10")
    {
        *number = 10;
    }
    else
    {
        *number = 404;
    }
}