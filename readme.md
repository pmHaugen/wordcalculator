# Word Calculator!
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


This is a word calculator that lets the user type in the words for numbers and operators and get the calculation back as output.


## Install

![alt picture](nchange.png)

Run the "make" command to create the a run file for the program.
```
make
```



## Usage

After running the make command you can use the "make run" command to run the program.
The words that you can write is one, two three, four, five, six, seven, eight, nine and ten. You have to write them exactly like that. No capital letters.
You can also write operators like this: plus, minus, over, times.

Just using numbers and operators like this: 2+2. also works.

Follow on screen description.
 
```
make run
```



## Contributing



Vipps me on +47 9#######


## License


MIT © Richard McRichface
